#!/usr/bin/env python3

import argparse
import os
from pathlib import Path

def get_precompile_definition_from_name(className):
    return className.upper() + '_H'

def get_header_file_from_name(className):
    return className.lower() + '.h'

def get_cpp_file_from_name(className):
    return className.lower() + '.cpp'


def make_class_header_string(className, noCopy) -> str:
    classDefine = get_precompile_definition_from_name(className)
    
    hStr = '#ifndef ' + classDefine + '\n'
    hStr += '#define ' + classDefine + '\n\n'
    hStr += 'class ' + className + ' {\n'
    hStr += 'public:\n'
    hStr += '\t' + className + '();\n'
    hStr += '\t~' + className + '();\n\n'
    hStr += '\t' + className + ' (const ' + className \
            + ' & l)'
    if noCopy:
        hStr += '=delete'
    hStr += ';\n'
    hStr += '\t' + className + ' (' + className \
            + ' && r);\n\n'
    hStr += '\t' + className + ' & operator= (const ' + className + ' & l)'
    if noCopy:
        hStr += '=delete'
    hStr += ';\n'
    hStr += '\t' + className + ' & operator= (' + className + ' && r);\n\nprivate:\n\n'
    
    hStr += '};\n\n'
    hStr += '#endif //' + classDefine + '\n'
    
    return hStr


def make_class_cpp_string(className, noCopy) -> str:
    
    classFileNameH = get_header_file_from_name(className)
    
    cStr = '#include \"' + classFileNameH + '\"\n\n'
    
    pre = className + '::'
    
    cStr += pre + className + '() {\n\t\n}\n\n'
    cStr += pre + '~' + className + '() {\n\t\n}\n\n'
    
    if not noCopy:
        cStr += pre + className + '(const ' \
                + className + ' & l) {\n\t\n}\n\n'
    
    cStr += pre + className + '(' + className \
            + ' && r) {\n\t\n}\n\n'
    
    if not noCopy:
        cStr += className + ' & ' + pre + 'operator= (const ' + className \
                + ' & l) {\n'
        cStr += '\treturn *this;\n}\n\n'
    
    cStr += className + ' & ' + pre + 'operator= (' + className + ' && r) {\n'
    cStr += '\treturn *this;\n}\n\n'
    
    return cStr


def save_class_header_string(className, noCopy, filepath, force):
    classFileNameH = get_header_file_from_name(className)
    hStr = make_class_header_string(className, noCopy)
    print(hStr)
    printToFile(classFileNameH, hStr, filepath, force)
    

def save_class_cpp_string(className, noCopy, filepath, force):
    classFileNameCpp = get_cpp_file_from_name(className)
    cStr = make_class_cpp_string(className, noCopy)
    print(cStr)
    printToFile(classFileNameCpp, cStr, filepath, force)


def make_class(className, noCopy, filepath, force):
    print('Making class ' + className)
    if noCopy:
        print('Making non-copy class.\n')

    print('\n\n::::::::\n\n')
    
    save_class_header_string(className, noCopy, filepath, force)
    save_class_cpp_string(className, noCopy, filepath, force)

    print('\n::::::::\n\n')
    

def printToFile(filename, msg, filepath, force):
    
    fullpath = filename
    if filepath != None:
        fullpath = os.path.join(filepath, filename)
        
    print(fullpath)
        
    if os.path.isfile(fullpath) and not force:
        print('File exists.  Overwrite? [y/n]: ')
        choice = input().lower()
        if choice != 'y':
            sys.exit('Error, cannot write: file exists.')
    
    with open (fullpath, 'w') as oFile:
        oFile.write(msg)

def main():

    parser = argparse.ArgumentParser(description='Create a C++ class with specified settings.')
    parser.add_argument('classNames', type=str, nargs='+', help='The name of the class. Recommend CamelCase.')
    parser.add_argument('-c', '--no-copy', dest='no_copy', action='store_true', help='Implements class with deleted copy-constructor and copy-assignment.')
    parser.add_argument('-f', '--force', action='store_true', help='Force overwrites and such.')
    parser.add_argument('-p', '--default-path', type=str, dest='default_path', default=None, help='Provides path for creating both files.')

    args = parser.parse_args()
        
    if args.default_path != None:
        assert os.path.isdir(args.default_path) == True
        assert os.path.exists(args.default_path) == True

    for className in args.classNames:
        make_class(className, args.no_copy, args.default_path, args.force)
        print('Class ' + className + ' constructed.')

    print('Finished writing all classes.')

if __name__ == "__main__":
    main()
