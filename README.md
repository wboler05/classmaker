# Class Maker

## Description

The purpose of this python script is fairly straightfoward.  I wrote it while I was laying in the bath and getting fed up with re-typing out class definitions and files for a simple C++ project that I had in my head.  How was I typing in the bath?  On my phone, of course.  So, rather than getting things taken care of, I instead wrote this script to ensure that I'll never be distracted again by nonsense...

Oh yea, what is this script?  It will simply create a class with the "Big 5" implemented, as well as a .cpp file to go along with the .h.  All files created should be compilable.  

## Disclaimer

Did I check to see of a library or script as such already exists on the WWW?  Obviously, no.  I am lazy.  Did I copy this from someone else?  Obviously, no, I'm too lazy to hunt down people's stuff. Can you use this in your project?  Of course, but don't blame me if it's broken. Are you perturbed?  Don't care.

## Usage

```ruby
# Execute script to create full class definition
python class_maker.py <ClassName>
# Execute script with multiple classes
python class_maker.py <ClassName1> <ClassName2> ... <ClassNameN>
# Execute script to make non-copyable class (delete copy-)
python class_maker.py <ClassName> -c
# Force an overwrite
python class_maker.py <ClassName> -f
# Write to an existing path
python class_maker.py <ClassName> -p <path_to_write_to>
```

## Future Implementations

Since I wrote the script in a hurry, it will appear to be unreadable and lacking sophistication.  The duration of this project should resemble as such.  I don't care too much to revisit a project after it's done what I wanted it to do.  So with that said, what will I add? 

* ~~Path definition through -p <path to save to>~~
* ~~Multiple class implementations i.e. -l ClassName1 -l ClassName2 ... etc, or some variation~~
* Differentiating between *src* path and *inc* path.
* A big MAYBE - Ability to add members via a member list (<member_name>:<datatype/class_name>)


## What is a "Big 5"???

What, you may ask, are the "Big 5"?  I'd go into full detail from memory, but I'd probably get it wrong.  I'm not going to read any documentation to double check my work either.  So, I'll just spit out some garbage here and if you're interested in seeing the correct information, read [here: (Rule of 3/5/0)]('https://en.cppreference.com/w/cpp/language/rule_of_three'). 

### Some pre-reqs
Let's hop to it. Everything in C++ is pass by value.  With that said, you can pass references and pointers as unsigned values, representing addresses in memory.  Imagine the difference between passing an entire class or struct, full of its members and lists, versus passing a simple address reference.  The difference is tremendous and offers quite some performance enhancement. 

So, if you're confused with C++ syntax, seeing "const Object & name" is really just saying "Give me a constant reference to some type of Object and store the reference in variable 'name'." That's nifty, right?  It's almost the same thing as passing "Object name", except that "Object name" will create an entire COPY and pass that COPY to the function.  Ugh.  Don't do that. 

### Big 5
Alright, we're talking about a lot of good basics, but what is this Big 5 thing?  Let's get to it. The **Big 5** are as follows


```ruby
~ClassName() 								// Destructor
ClassName(const ClassName & )				// Copy-Constructor
ClassName(ClassName && ) 					// Move-Constructor
ClassName & operator= (const ClassName & )	// Copy-Operator
ClassName & operator= (ClassName && )		// Move-Operator
```

Basically, if your class contains any sort of data structures that need special handling, such as pointers and lists, you should and are encouraged to implement the Big 5 to define how your variables are passed around.  In many cases, you may find that you must delete your copy constructor and operator completely and rely solely on move semantics.  Copy and Move?  Wait, what?...

If you check out the argument list in each of those examples, you will see that there is an explicite difference between copy and move semantics.  Copy is defined by "const ClassName &" and move is defined by "ClassName &&".  Why is this?  There's something that I didn't tell you earlier about constant reference.  Let's answer with a question: can you have a constant reference to a variable that no longer exists?  The answer is "yes" but that reference you have will no longer point to the data you're expecting. In short, "const ClassName &" is expecting for there to exist a variable in memory, and not on the stack or in a register (okay, I could be in the weeds here).  At this point, the move semantic "ClassName &&" comes to play by expecting a reference to a temporary variable. 

Alright, you might have understood that last part, or might not.  That's great.  Maybe an example will help.  Let's say you have the following: 

```ruby
Object A;
Object * B = new Object(A);
...
delete B;
```

Object A has been created with its default constructor.  A new Object is being created from A and stored to memory.  Object(A) is creating this new object using the copy-constructor.  The pointer to the new Object is being passed to B. So, we have used the Default Constructor and Copy Constructor. But, with B being a pointer, we must remember to delete it to avoid memory leaks. 'delete' will call the defined destructor for Object.  

Let's look at another example. 

```ruby
Object A;
Object B = Object(A);
```

We're still using Object's default constructor to create A, but now we're doing something different for B.  Object(A) is using the copy-constructor again, but it is storying that object to a temporary location.  B is being created by calling the move-operator with the temporary object as the parameter.  At least in this case, we won't have to worry about deleting B, but we can implement this in a better way.

```ruby
Object A;
Object B(A);
```

In this case, we're calling the copy constructor directly on B. Alright, let's look at a weird one:

```ruby
Object A(Object());
```

This is one of those "Why would I do this" sort of examples, but I'm showing you the move constructor, so deal with it. 

### Copy vs Move

Basically, your variable must exist in the scope of where the constant reference is being passed.  If it's a temporary variable defined within a function, and you leave the function, that constant reference will no longer exist.  If it's a temporary variable created and passed into a calling function, then it will exist for the full duration of that function call.  Here's an example of a constant reference failing.
```ruby
const double & ClassName::function_do_something(const double & a, const double & b) {
	double temp = a * b;
	return temp
}
```

In this example, both *a* and *b* will exist in this scope, even if temp variables are passed to *a* and *b*.  What will not exist is that *temp* variable being returned.  The function definition calls for a constant-reference, but temp is being declared locally on the stack.  It is not gauranteed to exist when you leave the function.
